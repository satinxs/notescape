
class NaiveMemoryStore {
    static CACHE_LIMIT = 1000;

    constructor() {
        this.cache = new Map();
    }

    clear() {
        this.cache.clear();
    }

    cleanupCache() {
        const cutoffDate = [...this.cache.values()].map(c => c.createdAt).sort().slice(-(NaiveMemoryStore.CACHE_LIMIT + 1))[0];
        this.cache = new Map([...this.cache.entries()].filter(([_, v]) => v.createdAt > cutoffDate));
    }

    set(path, value) {
        this.cache.set(path, { value, createdAt: Date.now() });

        if (this.cache.size > NaiveMemoryStore.CACHE_LIMIT)
            setTimeout(() => this.cleanupCache(), 0); //Make this non-blocking. Probably we might lose some cache entries, but it's not a great loss.
    }

    has(path) { return this.cache.has(path); }

    get(path) { return this.cache.get(path).value; }
}

class DummyMemoryStore {
    clear() { }
    set(path, value) { }
    has(path) { return false; }
    get(path) { return null; }
}

module.exports = { NaiveMemoryStore, DummyMemoryStore };