const { serialize, deserialize } = require('v8');

const createColumn = (name, type) => { name, type };


// Operators:
// //Simple comparison
// $eq
// $neq

// $gt
// $gte
// $lt
// $lte

// //String
// $like

// //Array query ( 'a': {$in: ['a','b','c']} )
// $in
// $nin

// //Query operators
// $and
// $or
// $not

// $exists

// //Array operators
// $all
// $any
// $none

// //Array & String
// $length


class Table {
    columns = new Map();
    indices = new Map();
    rows = [];

    constructor(name, persistedIndices = false) {
        this.name = name;
        this._persistedIndices = persistedIndices;
    }

    serialize(useJson = false) {
        const columns = this.columns.entries();
        const indices = this.indices.entries();
        const rows = this.rows;

        if (useJson)
            return JSON.stringify({ columns, indices, rows });

        return serialize({ columns, indices, rows });
    }

    deserialize(data, useJson = false) {
        const decoded = useJson ? JSON.parse(data) : deserialize(data);

        this.columns = new Map(decoded.columns);
        this.indices = new Map(decoded.indices);
        this.rows = new Map(decoded.rows);
    }
}

// class QueryBuilder {

// }

class TrieNode {
    value = [];
    children = new Map();

    addValue(n) { this.value.push(n); }
}

class Trie {
    root = new TrieNode();
    references = new Map();

    addReference(b, node) {
        if (!this.references.has(b))
            this.references.set(b, []);
        this.references.get(b).push(node);
    }

    insert(key, value) {
        const bytes = Buffer.from(key, 'utf-8');

        let node = this.root;
        for (let i = 0; i < bytes.length; i += 1) {
            const b = bytes[i];
            if (!node.children.has(b)) {
                const newNode = new TrieNode();
                this.addReference(b, newNode);
                node.children.set(b, newNode)
            }
            node = node.children.get(b);
        }

        node.addValue(value);
    }

    get(key) {
        const bytes = Buffer.from(key, 'utf-8');

        let node = this.root;
        for (let i = 0; i < bytes.length; i += 1) {
            const b = bytes[i];
            if (!node.children.has(b)) return [];
            node = node.children.get(b);
        }

        return node.value;
    }

    find(match) {
        const results = [];
        const startNodes = [];
        const seenNodes = new Set();

        if (match.startsWith('*')) {

        }
    }
}

module.exports = { Trie };