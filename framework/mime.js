const mimeTypes = {
    "application/json": ["json", "map"],
    "application/javascript": ["js"],
    "text/css": ["css"],
    "text/html": ["html"],
    "text/plain": ["txt"],
    "image/gif": ["gif"],
    "image/jpeg": ["jpeg", "jpg"],
    "image/png": ["png"],
    "image/svg+xml": ["svg"]
};

const _extToMimeType = new Map(Object.entries(mimeTypes).map(([k, arr]) => arr.map(v => [v, k])).reduce((p, c) => [...p, ...c], []));
// const _mimeToExt = new Map(Object.entries(mimeTypes).map(([k, arr]) => arr.map(v => [k, v])).reduce((p, c) => [...p, ...c], []));

const mimeTypeFromExtension = ext => {
    if (ext.startsWith('.'))
        ext = ext.substring(1);

    return _extToMimeType.get(ext);
}

module.exports = { mimeTypeFromExtension };