const Route = require('./Route');
const { DummyMemoryStore } = require('./Cache');

class RouterNode {
    children = new Map();
    value = null;
}

module.exports = class Router {
    constructor(memoryStore) {
        this.routes = [];
        this.middleware = [];

        this.memoryStore = memoryStore ?? new DummyMemoryStore();
    }

    addMiddleware(middleware) { this.middleware.push(middleware); }
    register(method, path, ...callbacks) { this.routes.push(new Route(this, method, path, callbacks)); }

    build() {
        const paths = this.routes.sort((a, b) => `${a.method}:${a.path}` < `${b.method}:${b.path}` ? -1 : 1);

        const tree = new RouterNode();
        for (const path of paths) {
            const parts = path.parts;

            let node = tree;
            for (let i = 0; i < parts.length; i += 1) {
                if (!node.children.has(parts[i]))
                    node.children.set(parts[i], new RouterNode());
                node = node.children.get(parts[i]);
            }
            node.value = path;
        }

        this.tree = tree;
    }

    match(ctx) {
        const fullPath = (ctx.method.toLowerCase() + ctx.pathname);

        if (this.memoryStore.has(fullPath))
            return this.memoryStore.get(fullPath);

        const parts = fullPath.split('/');
        const params = [];

        let node = this.tree;
        while (parts.length > 0) {
            const part = parts.shift();

            if (node.children.has(part))
                node = node.children.get(part);
            else if (node.children.has(Route.wildcard)) {
                node = node.children.get(Route.wildcard);
                params.push(part);
            } else {
                //We also store fails in the cache.
                this.memoryStore.set(fullPath, null);
                return null;
            }
        }

        if (node.value) {
            const final = node.value.materialize(params);
            this.memoryStore.set(fullPath, final);
            return final;
        }

        return null;
    }
};
