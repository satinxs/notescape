const http = require('http');
const Path = require('path');

const Context = require('./Context');
const Router = require('./Router');
const { callbackChain } = require('./utilities');

module.exports = class Application extends Router {
    constructor(config = {}) {
        super(config.memoryStore);
    }

    start(port, callback) {
        this.build();
        this.middleware.push(async () => { }); //Add a dummy function, to allow middleware in any part of the chain to freely call next.
        this.noMatchHandler = callbackChain(this.middleware);

        this.server = http.createServer(async (req, res) => await this.listen(req, res));

        this.server.on('upgrade', (req, socket) => this.handleUpgrade(req, socket));

        this.server.listen(port, callback);
    }

    async handleUpgrade(request, socket) {

    }

    async listen(req, res) {
        const ctx = new Context(req, res);

        const route = this.match(ctx);
        if (!route) {
            await this.noMatchHandler(ctx);
        } else {
            await ctx.initialize(route.params);
            await route.handler(ctx);
        }

        if (!ctx.response.finished)
            ctx.response.end();
    }

    use(middleware) {
        this.addMiddleware(middleware);
        return this;
    }
    mount(base, router) {
        for (const route of router.routes) {
            const { method, path, handler } = route;
            const newPath = Path.join(base, path).split(Path.sep).join('/');
            this.register(method, newPath, handler);
        }
        return this;
    }
    get(path, ...callbacks) { this.register('get', path, ...callbacks); }
    post(path, ...callbacks) { this.register('post', path, ...callbacks); }
    put(path, ...callbacks) { this.register('put', path, ...callbacks); }
    delete(path, ...callbacks) { this.register('delete', path, ...callbacks); }
}; 