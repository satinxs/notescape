const { is, Type } = require("../Validator");

class Document {
    constructor(name) {
        if (name && is.String(name) && name.length > 0) {
            this.#collectionName = name;
        }
    }

    fromSchema(schema) {
        
    }
}

class User extends Document {
    email = { type: String, required: true, unique: true };
    name = String;
    pwdHash = { type: String, required: true };
    pwdSalt = { type: String, required: true };
    createDate = Date;
    lastSeen = Date;

    constructor() {
        super('users');
    }
}