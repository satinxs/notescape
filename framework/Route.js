const { callbackChain } = require('./utilities');

const wildcard = Symbol('*');

class Route {
    constructor(router, method, path, callbacks) {
        this.method = method;
        this.path = path;
        this.handler = callbackChain([...router.middleware, ...callbacks]);

        this.parts = [];
        this.params = [];

        this.parsePathParams();
    }

    parsePathParams() {
        const parts = (this.method + this.path).split('/');

        this.parts = []; this.params = [];
        for (let i = 0; i < parts.length; i += 1) {
            const part = parts[i];
            const match = part.match(/{(\w+)}/);
            if (match && match[1]) {
                this.parts.push(wildcard);
                this.params.push(match[1]);
            } else
                this.parts.push(part);
        }
    }

    materialize(values) {
        return {
            params: new Map(values.map((v, i) => [this.params[i], decodeURI(v)])),
            handler: this.handler
        };
    }
}

Route.wildcard = wildcard;

module.exports = Route;