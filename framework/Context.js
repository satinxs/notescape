const URL = require('url');
const fs = require('fs/promises');

const mime = require('./mime');

const _getStatus = (code, obj) => code ?? ((obj === null) ? 204 : 200);

module.exports = class Context {
    constructor(req, res) {
        this.request = req;
        this.response = res;
        this.body = null;

        const p = URL.parse(req.url);
        this.pathname = p.pathname;
        this.query = new Map(new URLSearchParams(p.query).entries());
    }

    get contentLength() { return parseInt(this.request.headers['content-length']); }
    get method() { return this.request.method; }
    get url() { return this.request.url; }
    get headers() { return this.request.headers; }

    async initialize(params) {
        if (params.size > 0)
            this.params = params;

        if (this.contentLength > 0) {
            if (this.request.headers['content-type'] === 'application/json')
                await this._processJsonBody();
        }
    }

    async _processJsonBody() {
        const chunks = [];
        for await (const chunk of this.request)
            chunks.push(chunk);

        const string = Buffer.concat(chunks).toString();
        this.body = JSON.parse(string);
    }

    setStatus(status) {
        this.status = status;
        return this;
    }

    addHeader(name, value) {
        this.response.setHeader(name, value);
        return this;
    }

    send(obj = null) {
        this.setStatus(_getStatus(this.status, obj));
        this.response.writeHead(this.status, { 'Content-Type': mime.mimeTypeFromExtension('json') });
        this.response.end(JSON.stringify(obj));
    }

    async sendFile(fileName) {
        try {
            const file = await fs.open(fileName, "r");
            const stat = await file.stat();
            this.response.addHeader('Content-Size', stat.size);
            const stream = file.createReadStream();
            stream.pipe(this.response);
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
};