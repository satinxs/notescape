const { Stream } = require('stream')

function callbackChain(funcs) {
    const handler = funcs.pop();

    return funcs
        .reverse()
        .reduce((p, c) => async ctx => await c(ctx, async () => await p(ctx)), handler);
};

const isStream = obj => obj instanceof Stream;

const isReadable = obj => isStream(obj) && typeof (obj._read) === 'function' && typeof obj._readableState === 'object';
const isWritable = obj => isStream(obj) && typeof (obj._write) === 'function' && typeof obj._writableState === 'object';

const isDuplex = obj => isReadable(obj) && isWritable(obj);

const groupBy = (array, fn) => array.reduce((g, c) => {
    const key = fn(c);
    g[key] = g[key] ?? [];
    g[key].push(c);
    return g;
}, {});

module.exports = { isStream, isReadable, isWritable, isDuplex, groupBy, callbackChain };
