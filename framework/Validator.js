const isObject = v => typeof v === 'object' && v !== null;
const isString = v => typeof v === 'string';
const isNumber = v => typeof v === 'number';
const isBoolean = v => typeof v === 'boolean';

const isDate = v => (isNumber(v) && Number.isInteger(v)) || (isObject(v) && v instanceof Date) || typeof Date.parse(v) === 'number';
const isBuffer = v => isObject(v) || v instanceof Buffer;
const isArray = v => Array.isArray(v);
const isMap = v => isObject(v) && v instanceof Map;
const isSet = v => isObject(v) && v instanceof Set;

const is = {
    Object: isObject,
    String: isString,
    Number: isNumber,
    Boolean: isBoolean,
    Date: isDate,
    Buffer: isBuffer,
    Array: isArray,
    Map: isMap,
    Set: isSet
};

// const isDocument = function (m) {
//     return m && m.documentClass && m.documentClass() === 'document';
// };

// const isEmbeddedDocument = function (e) {
//     return e && e.documentClass && e.documentClass() === 'embedded';
// };

// const isReferenceable = function (r) {
//     return isDocument(r) || isNativeId(r);
// };

// const isNativeId = function (n) {
//     return DB().isNativeId(n);
// };

// const isSupportedType = function (t) {
//     return (t === String || t === Number || t === Boolean ||
//         t === Buffer || t === Date || t === Array ||
//         isArray(t) || t === Object || t instanceof Object ||
//         typeof (t.documentClass) === 'function');
// };

// const isType = function (value, type) {
//     if (type === String) {
//         return isString(value);
//     } else if (type === Number) {
//         return isNumber(value);
//     } else if (type === Boolean) {
//         return isBoolean(value);
//     } else if (type === Buffer) {
//         return isBuffer(value);
//     } else if (type === Date) {
//         return isDate(value);
//     } else if (type === Array || isArray(type)) {
//         return isArray(value);
//     } else if (type === Object) {
//         return isObject(value);
//     } else if (type.documentClass && type.documentClass() === 'document') {
//         return isDocument(value) || DB().isNativeId(value);
//     } else if (type.documentClass && type.documentClass() === 'embedded') {
//         return isEmbeddedDocument(value);
//     } else if (type === DB().nativeIdType()) {
//         return isNativeId(value);
//     } else {
//         throw new Error('Unsupported type: ' + type.name);
//     }
// };

// const isValidType = function (value, type) {
//     // NOTE
//     // Maybe look at this: 
//     // https://github.com/Automattic/mongoose/tree/master/lib/types

//     // TODO: For now, null is okay for all types. May
//     // want to specify in schema using 'nullable'?
//     if (value === null) return true;

//     // Issue #9: To avoid all model members being stored
//     // in DB, allow undefined to be assigned. If you want
//     // unassigned members in DB, use null.
//     if (value === undefined) return true;

//     // Arrays take a bit more work
//     if (type === Array || isArray(type)) {
//         // Validation for types of the form [String], [Number], etc
//         if (isArray(type) && type.length > 1) {
//             throw new Error('Unsupported type. Only one type can be specified in arrays, but multiple found:', + type);
//         }

//         if (isArray(type) && type.length === 1 && isArray(value)) {
//             let arrayType = type[0];
//             for (let i = 0; i < value.length; i++) {
//                 let v = value[i];
//                 if (!isType(v, arrayType)) {
//                     return false;
//                 }
//             }
//         } else if (isArray(type) && type.length === 0 && !isArray(value)) {
//             return false;
//         } else if (type === Array && !isArray(value)) {
//             return false;
//         }

//         return true;
//     }

//     return isType(value, type);
// };

// const isInChoices = function (choices, choice) {
//     if (!choices) {
//         return true;
//     }
//     return choices.indexOf(choice) > -1;
// };

// const isEmptyValue = function (value) {
//     return typeof value === 'undefined' || (!(typeof value === 'number' || value instanceof Date || typeof value === 'boolean')
//         && (0 === Object.keys(value).length));
// };

class Type {
    static String({ required } = {}) { return new Type({ type: 'string', unique, required }); }
    static Boolean({ required } = {}) { return new Type({ type: 'boolean', unique, required }); }
    static Number({ required } = {}) { return new Type({ type: 'number', unique, required }); }
    static ArrayOf(type, { required } = {}) { return new Type({ type: 'array', subtype: type, required }); }

    constructor({ type, subtype, required } = {}) {
        this._type = type;
        this._subtype = subtype;
        this._required = required;
    }

    required(flag = true) {
        this._required = flag;
        return this;
    }

    setType(type, subtype) {
        this._type = type;
        this._subtype = subtype;
        return this;
    }

    toValidator() {
        switch (this._type) {
            case 'string':
            default:
        }
    }
}

// class Validator {
//     fields = new Map();

//     static fromSchema(schema) {
//         const v = new Validator();
//     }

//     addField(name, validator) {
//         fields.set(name, validator);
//         return this;
//     }

// }

module.exports = { Type, is };
