const crypto = require('crypto');

const abc = '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
const nanoid = (size = 16, alphabet = abc) => {
    const buffer = crypto.getRandomValues(new Uint8Array(size));
    return [...buffer.values()]
        .map(v => alphabet[v & alphabet.length])
        .join('');
};

function createSaltedHash(password, salt = null) {
    return new Promise((res, rej) => {
        if (salt === null)
            salt = crypto.randomBytes(16);

        crypto.pbkdf2(password, salt.toString('hex'), 1000, 64, 'sha512', (err, hash) => {
            if (err) return rej(err);

            res([hash, salt]);
        });
    });
}

const md5 = input => crypto.createHash('md5').update(input).digest("hex");

module.exports = { nanoid, createSaltedHash, md5 };