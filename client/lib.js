export const $ = id => document.getElementById(id);

const abc = '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
export const nanoid = (size = 16, alphabet = abc) => {
    const buffer = crypto.getRandomValues(new Uint8Array(size));
    return [...buffer.values()]
        .map(v => alphabet[v & alphabet.length])
        .join('');
};

export function hash(key) {
    if (!isString(key)) throw new Error('Unexpected key type. Should provide a string.');

    const bytes = new TextEncoder().encode(key);

    let h = 0;
    for (let i = 0; i < bytes.length; i += 1) {
        h += bytes[i];
        h += h << 10;
        h += h >> 6;
    }

    h += h << 3;
    h ^= h >> 11;
    h += h << 15;

    return h;
};