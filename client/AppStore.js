import { makeAutoObservable } from 'mobx';
import { nanoid } from './lib';

export default class AppStore {
    isEditing = false;
    todos = [];
    selected = null;

    constructor() { makeAutoObservable(this); }

    save(item) {
        if (item.id) {
            const todo = this.todos.find(todo => todo.id === item.id);
            todo.title = item.title;
        } else {
            item.id = nanoid();
            this.todos.push(item);
        }

        this.selected = null;
        this.isEditing = false;
    }

    select(id) {
        const item = this.todos.find(todo => todo.id === id);
        this.selected = item;
        this.isEditing = true;
    }

    startEditing() {
        this.isEditing = true;
    }
}