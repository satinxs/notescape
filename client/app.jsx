import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import { observer } from 'mobx-react-lite';

import { $ } from './lib';
import AppStore from './AppStore';

const store = new AppStore();

const EditToDo = ({ store }) => {
  const initialTitle = store.selected ? store.selected.title : '';
  const [title, setTitle] = useState(initialTitle);

  const save = ev => {
    ev.preventDefault();
    store.save({ id: store.selected?.id, title });
  };

  return <form>
    <input type="text" value={title} onChange={ev => setTitle(ev.target.value)} autoFocus={true} />
    <button onClick={save}>Save</button>
  </form>
};

const App = observer(({ store }) => {
  if (store.isEditing)
    return <EditToDo store={store} />;

  return <div>
    <ul>
      {store.todos.map(todo => (
        <li key={todo.id} onClick={() => store.select(todo.id)}>{todo.title}</li>
      ))}
    </ul>
    <button onClick={() => store.startEditing()} >Add</button>
  </div>;
});

ReactDOM
  .createRoot($('root'))
  .render(<App store={store} />);
