async function log(ctx, next) {
    const start = new Date().getTime();
    await next();
    const end = new Date().getTime();
    console.log(`[${ctx.method}] ${ctx.url}`, end - start, 'ms', '->', ctx.status);
};

module.exports = { log };