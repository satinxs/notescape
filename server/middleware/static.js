const fs = require('fs/promises');
const path = require('path');
const { mimeTypeFromExtension } = require('../../framework/mime');

function static(baseFolder) {
    const basePath = path.isAbsolute(baseFolder) ? path.normalize(baseFolder) : path.join(process.cwd(), baseFolder);

    const illegalCharacters = /[\?<>\\:\*\|"]/g;
    const controlCharacters = /[\x00-\x1f\x80-\x9f]/g;
    const windowsReservedFileNames = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
    //Validates filename and reports error if filename contains:
    // Illegal Characters on Various Operating Systems like / ? < > \ : * | " ( https://kb.acronis.com/content/39790 )
    // Unicode controls like C0 0x00-0x1f & C1 (0x80-0x9f) ( http://en.wikipedia.org/wiki/C0_and_C1_control_codes )
    // Reserved filenames: ".", "..", "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9","LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", and "LPT9"
    function isValidPath(p) {
        if (illegalCharacters.test(p) || controlCharacters.test(p) || windowsReservedFileNames.test(p))
            return false;

        const fullPath = path.join(basePath, p);
        if (path.relative(basePath, fullPath).startsWith('..'))
            return false;

        return true;
    }

    return async (ctx, next) => {
        const filePath = ctx.pathname === '/' ? '/index.html' : ctx.pathname;
        const fullPath = path.join(basePath, filePath);

        try {
            if (isValidPath(filePath)) {
                //`access` throws if it fails.
                await fs.access(fullPath, fs.constants.R_OK);

                try {
                    const stat = await fs.stat(fullPath);

                    ctx.addHeader('Content-Length', stat.size);
                    ctx.addHeader('Content-Type', mimeTypeFromExtension(path.extname(filePath)));

                    ctx.setStatus(200);
                    ctx.response.writeHead(200);

                    const file = await fs.open(fullPath, 'r');
                    const stream = file.createReadStream();

                    await new Promise((res, rej) => {
                        stream.on('end', () => { res(); });
                        stream.on('error', error => rej(error));
                        stream.pipe(ctx.response);
                    });

                    return ctx.response.end();
                } catch (error) {
                    console.error(error);
                }
            }
        } catch { } //We don't care about the fs.access error, just means that the file doesn't exist.

        //If the path is not valid or the file does not exist, we continue to the next middleware.
        await next();
    };
}

module.exports = { static };