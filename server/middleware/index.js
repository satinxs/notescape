const { log } = require('./log');
const { auth } = require('./auth');
const { static } = require('./static');

module.exports = { log, auth, static };