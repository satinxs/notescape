const config = require('./config.json');
const { NaiveMemoryStore } = require('../framework/Cache');
const Application = require('../framework/Application');
const { log, static } = require('./middleware');
const users = require('./routes/userRoutes');

const app = new Application({ memoryStore: new NaiveMemoryStore() })
    .use(log)
    .mount('/api', users)
    .use(static(config.public))
    .use(async ctx => {
        //If no other route/middleware stopped the call chain by now, we can safely assume a 404.
        ctx.setStatus(404, 'Not Found');
        ctx.send({ message: 'Not found' });
    });

app.start(config.port, () => {
    console.log('Listening on port', config.port);
});
