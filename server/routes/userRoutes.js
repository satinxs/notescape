const Application = require('../../framework/Application');
const { UserLogic } = require('../logic/userLogic');
const { auth } = require('../middleware');

const users = new Application();

const logic = new UserLogic();

logic.notes = [];

users.get('/notes', async ctx => {
    ctx.send(logic.notes);
});

users.post('/notes', async ctx => {
    logic.notes = ctx.body;
});

// users.get('/users/{userId}/profile', auth, async ctx => {
//     const userId = ctx.params.get('userId');

//     const profile = await logic.getProfile(userId);

//     console.log(ctx);

//     ctx.send(profile);
// });

// users.post('/users/login', async ctx => {

// });

// users.post('/users/register', async ctx => {

// });

module.exports = users;